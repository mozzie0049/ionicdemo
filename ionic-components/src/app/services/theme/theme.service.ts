import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  // definition theme (主题)
  private theme: BehaviorSubject<string>;

  /**
   * constructor (初始化)
   * @memberof ThemeService
   */
  constructor() {
    this.theme = new BehaviorSubject('');
  }

  /**
   * set theme subscription function
   * 设置主题函数
   *
   * @param {string} val
   * @memberof ThemeService
   */
  set_theme_handler(val: string) {
    this.theme.next(val);
  }

  /**
   * get theme subscription function
   * 获取主题函数
   *
   * @returns
   * @memberof ThemeService
   */
  get_theme_handler() {
    return this.theme.asObservable();
  }
}
