import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ThemeService } from './services/theme/theme.service';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // definition theme (主题样式字符)
  public theme: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    // import theme service (导入主题设置)
    private themeService: ThemeService,
  ) {
    this.initializeApp();

    // update theme function (更新主题样式)
    this.update_current_theme_handler();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * update theme function
   * 更新主题样式函数
   *
   * @private
   * @memberof AppComponent
   */
  private update_current_theme_handler() {
    this.themeService.get_theme_handler().subscribe(val => {
      if (val === '') {
        this.storage.get('theme').then(t => {
          if (t === 'theme-light' || t === 'theme-dark') {
            this.theme = t;
          } else {
            this.theme = 'theme-light';
          }
        });
      } else {
        this.theme = val;
      }
    });
  }
}
