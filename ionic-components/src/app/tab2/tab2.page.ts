import { Component } from '@angular/core';
import { ThemeService } from '../services/theme/theme.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  // switching (切换中)
  public switching: boolean = false;
  // set dark theme (切换主题)
  public darkTheme: boolean;

  constructor(
    private themeService: ThemeService,
    private storage: Storage
  ) {
    this.init_darkTheme_handler();
  }

  /**
   * init darkTheme value
   * 初始化darkTheme值
   *
   * @private
   * @memberof Tab2Page
   */
  private init_darkTheme_handler() {
    this.storage.get('theme').then(val => {
      if (val) {
        this.darkTheme = (val === 'theme-dark');
      } else {
        this.darkTheme = false;
      }
    });
  }

  /**
   * switch theme function
   * 切换主题
   *
   * @memberof Tab2Page
   */
  public switch_theme_handler() {
    if (this.switching) {
      return;
    }

    this.switching = true;

    setTimeout(() => {
      let themeStr = this.darkTheme ? 'theme-dark' : 'theme-light';
      this.storage.set('theme', themeStr);
      this.themeService.set_theme_handler(themeStr);
      this.switching = false;
    }, 200);
  }

}
