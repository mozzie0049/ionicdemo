import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-action-sheet',
  templateUrl: './action-sheet.page.html',
  styleUrls: ['./action-sheet.page.scss'],
})
export class ActionSheetPage implements OnInit {

  public actionSheetMenu: any = {
    header: 'Albums',
    buttons: [{
      text: 'Delete',
      role: 'destructive',
      icon: 'trash',
      handler: () => {
        console.log('Delete clicked');
      }
    }, {
      text: 'Share',
      icon: 'share',
      handler: () => {
        console.log('Share clicked');
      }
    },
    {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  };

  constructor(
    private actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
  }

  /**
   * show action sheet handler
   *
   * @memberof ActionSheetPage
   */
  async show_action_sheet_handler() {
    const actionSheet = await this.actionSheetController.create(this.actionSheetMenu);
    await actionSheet.present();
  }

}
