import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.page.html',
  styleUrls: ['./badge.page.scss'],
})
export class BadgePage implements OnInit {

  public colorsArray: any = ['primary', 'secondary', 'tertiary', 'success', 'warning', 'danger', 'light', 'medium', 'dark'];
  public currentColor: string = this.colorsArray[0];

  constructor() { }

  ngOnInit() {
  }

  /**
   * switch color function
   *
   * @param {string} item
   * @memberof BadgePage
   */
  public switch_color_handler(item: string) {
    this.currentColor = item;
  }

}
