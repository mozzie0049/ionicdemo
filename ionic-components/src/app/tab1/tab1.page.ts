import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    private router: Router,
    // private navCtrl: NavController
  ) { }

  /**
   * show action sheet page
   *
   * @memberof Tab1Page
   */
  public goto_action_sheet_page_handler() {
    this.router.navigate(['/example/action-sheet']);
    // this.navCtrl.navigateForward('/example/action-sheet');
  }

  /**
   * show alert page
   *
   * @memberof Tab1Page
   */
  public goto_alert_page_handler() {
    this.router.navigate(['/example/alert']);
  }

  /**
   * show badge page
   *
   * @memberof Tab1Page
   */
  public goto_badge_page_handler() {
    this.router.navigate(['/example/badge']);
  }

  /**
   * show button page
   *
   * @memberof Tab1Page
   */
  public goto_button_page_handler() {
    this.router.navigate(['/example/button']);
  }

}
