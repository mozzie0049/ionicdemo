import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'example/action-sheet',
    loadChildren: () => import('./page/example/action-sheet/action-sheet.module').then(m => m.ActionSheetPageModule)
  },
  {
    path: 'example/alert',
    loadChildren: () => import('./page/example/alert/alert.module').then(m => m.AlertPageModule)
  },
  {
    path: 'example/badge',
    loadChildren: () => import('./page/example/badge/badge.module').then( m => m.BadgePageModule)
  },
  {
    path: 'example/button',
    loadChildren: () => import('./page/example/button/button.module').then( m => m.ButtonPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
